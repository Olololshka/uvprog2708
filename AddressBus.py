#!/usr/bin/env python
# -*- coding: utf-8 -*-

from uv_prog_pin import GPIO

dataBusPins = [
    67, # A0
    69, # A1
    45, # A2
    26, # A3
    23, # A4
    27, # A5
    47, # A6
    65, # A7
    61, # A8
    44  # A9
]

class uvprogAddressBus:
    def __init__(self):
        self.lines = []
        for l in dataBusPins:
            addressline = GPIO(l)
            addressline.set_dir('out')
            addressline.set_val(0)
            self.lines.append(addressline)

    def setAddress(self, addr):
        if addr > 0x3ff :
            print('Warning, address {:#x} is out of range!, cropping...')

        for i in range(10):
            v = (addr & (1 << i)) != 0
            self.lines[i].set_val(v)

