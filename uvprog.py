#!/usr/bin/env python
# -*- coding: utf-8 -*-


from DataBus import uvprogDataBus, test
from AddressBus import uvprogAddressBus
from uv_prog_pin import GPIO
import argparse
import sys

circuit_size = 0x400

WP_pin = 46 # PROG

def data_fill(d):
    if len(d) < circuit_size:
        return d + [ 0xff for x in range(circuit_size - len(d)) ]
    elif len(d) > circuit_size:
        return d[:circuit_size]
    return d

def read_circuit(A, D):
    result = []
    print('Reading circuit')
    raw_input('Select READ MODE (GND) Enter to continue.')
    for i in range(circuit_size):
        A.setAddress(i)
        v = D.read()
        sys.stdout.write('\rReading {:#x}: {:#x}'.format(i, v))
        sys.stdout.flush()
        result.append(v)
    print(' ')
    return bytearray(result)

def write_circuit(A, D, wp, data):
    d = list(bytearray(data))
    print('Writing circuit')
    raw_input('Select WRITE MODE (+27V) Enter to continue.')
    for i in range(circuit_size):
        A.setAddress(i)
        D.write(d[i])
        wp.set_val(0)
        sys.stdout.write('\rWriting {:#x}: {:#x}'.format(i, d[i]))
        sys.stdout.flush()
        wp.set_val(1)


def main():
    parser = argparse.ArgumentParser(description="Programmer for 2708 UV-EPROM")
    parser.add_argument('--file', '-f', type=str,  help='Bunary file name for load/store data')
    parser.add_argument('-r', action="store_true", help='Read EPROM context and store to file')
    parser.add_argument('-w', action="store_true", help='Write EPROM context from file')
    parser.add_argument('--compare', '-c', action="store_true", help='Compare EPROM data and file context')
    parser.add_argument('--test', '-t', action="store_true", help='Run tests')

    options = vars(parser.parse_args())
    if options['test']:
        print('Running the test of data bus. Make sure what socket is EMPTY!')
        raw_input('Enter to continue')
        test()
        return

    if options['w']:
        options['r'] = False
        options['compare'] = False

    if options['compare']:
        options['r'] = True

    databus = uvprogDataBus()
    addressBus = uvprogAddressBus()
    wp = GPIO(WP_pin)
    wp.set_dir('out')
    wp.set_val(1) # idle state

    print('================================================================================\r\n')

    if options['r']:
        if options['compare']:
            f = open(options['file'], 'rb')
            d = list(bytearray(f.read()))
            pattern = data_fill(d)
            f.close()
        data = read_circuit(addressBus, databus)
        if options['compare']:
            print('Verifying content')
            _n = 1
            err = False
            for i in range(len(data)):
                if pattern[i] != data[i]:
                    err = True
                    print('{}: {:#x}:\t{:#x} != {:#x}'.format(_n, i, data[i], pattern[i]))
                    _n = _n + 1
            if not err:
                print('Verifying successfull')
        else:
            fw = open(options['file'], 'wb')
            fw.write(data)
            fw.close()
    elif options['w']:
        f = open(options['file'], 'rb')
        d = list(bytearray(f.read()))
        data = data_fill(d)
        write_circuit(addressBus, databus, wp, data)
        f.close()
    else:
        print('No work mode specified, nothing to do!')
    print('\n\r==========================================================================')

if __name__ == '__main__':
    main()
