#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random

from data_pin_pair import uvprogDataPinPair, uvprogDataPins

class uvprogDataBus:
    def __init__(self):
        i = 0
        self.pins = []
        for pin in [ 'D{}'.format(x) for x in range(8) ]:
            self.pins.append(uvprogDataPinPair(pin))
        self.read()

    def write(self, data):
        if not (type(data) is int):
            raise TypeError('data mast be int')
        for i in range(8):
            self.pins[i].write((data & (1 << i)) != 0)
        r = self._raw_read()
        p = data & 0xff
        if r != p:
            raise RuntimeError("Verifying data bus state error {:#x} != {}".format(r, p))

    def read(self):
        res = 0
        for i in range(8):
            if self.pins[i].read():
                res = res | (1 << i)
        return res

    def _raw_read(self):
        res = 0
        for i in range(8):
            if self.pins[i]._raw_read():
                res = res | (1 << i)
        return res

    def test(self):
        for i in range(8):
            test_pattern = random.randint(0, 255)
            try:
                self.write(test_pattern)
            except RuntimeError:
                res = self._raw_read()
                print ('{} testing pass {} != {}'.format(i, res, test_pattern))
                return False
        return True


def test():
    bus = uvprogDataBus()
    if bus.test():
        print ('DataBus selftest PASSED')
    else:
        print ('DataBus selftest FAILED')

if __name__ == '__main__':
    test()


